export default {
  Home: '/',
  Game: '/new-game',
  CreateUser: '/create-user',
  Login: '/login',
  Logout: '/logout',
}