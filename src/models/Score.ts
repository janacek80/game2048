import { Player } from './Player';

export interface Score {
  player: Player;
  score: number;
};