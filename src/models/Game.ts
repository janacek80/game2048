export interface Game {
  state: Array<Array<number>>;
  score: number;
  finished: boolean;
}

export enum Direction {
  Left = 'Left',
  Right = 'Right',
  Up = 'Up',
  Down = 'Down',
}