import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from 'react-router-dom';
import './App.css';
import WelcomeScene from './scenes/Welcome';
import GameScene from './scenes/Game';
import CreateUserScene from './scenes/CreateUser';
import routes from './routes';
import LoginScene from './scenes/Login';
import LogoutScene from './scenes/Logout';

const App = () => {

  return (
    <Router>
      <div className="App">
        <Switch>
          <Route path={routes.Home} exact={true}>
            <WelcomeScene />
          </Route>
          <Route path={routes.Game} exact={true}>
            <GameScene />
          </Route>
          <Route path={routes.CreateUser} exact={true}>
            <CreateUserScene />
          </Route>
          <Route path={routes.Login} exact={true}>
            <LoginScene />
          </Route>
          <Route path={routes.Logout} exact={true}>
            <LogoutScene />
          </Route>
        </Switch>
      </div>
    </Router>
  );
};

export default App;
