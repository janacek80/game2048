import { useQuery } from '@apollo/client';
import React from 'react';
import { Link } from 'react-router-dom';
import { Score } from '../models/Score';
import { HighScoresQuery } from '../queries/Scores';
import { LoggedUserQuery } from '../queries/User';
import routes from '../routes';

const WelcomeScene = () => {
  const { loading, error, data } = useQuery(HighScoresQuery);
  const loggedUserQuery = useQuery(LoggedUserQuery, {
    fetchPolicy: 'network-only'
  });

  return (
    <div className="all-scores">
      {error &&
        <p className="alert">{error}</p>}
      {loading ? (
        <p>Loading data...</p>
      ) : (
          <>
            <p>Top 10</p>
            <table cellSpacing={0} cellPadding={0}>
              <tbody>
                {data?.allScores
                  .filter((score: Score) => score.score)
                  .filter((score: Score, index: number) => index < 10)
                  .map((score: Score, index: number) => (
                    <tr key={index}>
                      <td align="right">{index + 1}</td>
                      <td align="left">{score.player.name}</td>
                      <td align="right">{score.score}</td>
                    </tr>
                  ))}
              </tbody>
            </table>

            {!window.sessionStorage.getItem('token') &&
              <>
                <Link to={routes.Login} className="btn">
                  Log In
                </Link>
                <Link to={routes.CreateUser} className="btn inverse">
                  Register
                </Link>
              </>}
            {window.sessionStorage.getItem('token') &&
              <>
                {loggedUserQuery?.data?.authenticatedUser &&
                  <div style={{ marginBottom: 5 }}>
                    <small>Hello {loggedUserQuery?.data?.authenticatedUser.name}, nice to see you again!</small>
                  </div>}
                <Link to={routes.Game} className="btn">
                  New Game
                </Link>
                <Link to={routes.Logout} className="btn">
                  Logout
                </Link>
              </>}
          </>
        )}

    </div>
  );
};

export default WelcomeScene;