import React from 'react';
import { Redirect } from 'react-router-dom';
import routes from '../routes';

const LogoutScene = () => {
  window.sessionStorage.removeItem('token');

  return (
    <Redirect to={routes.Home} />
  );
};

export default LogoutScene;