import { useMutation, useQuery } from '@apollo/client';
import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { Direction, Game } from '../models/Game';
import { NewGameQuery, ProcessGameMutation } from '../queries/Game';
import { CreateScoreMutation } from '../queries/Scores';
import routes from '../routes';

const GameScene = () => {
  const { data, loading, error } = useQuery(NewGameQuery);
  const [processGameMutation] = useMutation(ProcessGameMutation);
  const [createScoreMutation] = useMutation(CreateScoreMutation);
  const [game, setGame] = useState<Game | null>(null);

  const gameRef = React.useRef(game);

  const setNewGame = (newGame: Game) => {
    setGame(newGame);
    gameRef.current = newGame;
  };

  useEffect(() => {
    setNewGame(data?.newGame);
  }, [data]);

  const processGame = (direction: Direction): void => {
    if (gameRef?.current) {
      if (gameRef?.current?.finished !== true) {
        processGameMutation({
          variables: {
            game: {
              state: gameRef.current.state,
              score: gameRef.current.score,
              direction
            }
          }
        })
          .then((result) => result.data.processGame)
          .then((newGameData) => {
            setNewGame(newGameData);
            // finished
            if (newGameData.finished === true) {
              createScoreMutation({
                variables: {
                  data: {
                    score: newGameData.score,
                  }
                }
              });
            }
          });
      }
    }
  };

  const handleKeyDown = (event: KeyboardEvent): void => {
    switch (event.key) {
      case 'ArrowLeft':
        processGame(Direction.Left);
        break;
      case 'ArrowRight':
        processGame(Direction.Right);
        break;
      case 'ArrowUp':
        processGame(Direction.Up);
        break;
      case 'ArrowDown':
        processGame(Direction.Down);
        break;
    }
  };

  useEffect(() => {
    window.addEventListener("keydown", handleKeyDown);
    return (() => {
      window.removeEventListener('keydown', handleKeyDown, true);
    });
    // eslint-disable-next-line
  }, []);

  if (error) {
    return (
      <>
        <p className="alert">Chyba! {error.message}</p>
        <div className="game-footer">
          <Link to={routes.Home} className="btn">
            Home
          </Link>
        </div>
      </>
    );
  }

  if (loading) {
    return <p>Creating new Game ...</p>;
  }

  return (
    <>
      <div className="game-score">
        Your Score: <strong>{game?.score}</strong>
      </div>
      {game?.finished &&
        <div>Game Over</div>}
      <div className="game-grid">
        {game?.state.map((rowNums: Array<number>, rowIndex: number) => (
          <div className="game-grid__row" key={rowIndex}>
            {rowNums?.map((num: number, cellIndex: number) => (
              <div
                key={cellIndex}
                className="game-grid__cell"
                dangerouslySetInnerHTML={{ __html: `${num !== 0 ? num : `&nbsp;`}` }}
              />
            ))}
          </div>
        ))}
      </div>
      <div className="game-footer">
        <Link to={routes.Home} className="btn">
          Home
        </Link>
      </div>
    </>
  );
};

export default GameScene;