import { useMutation } from '@apollo/client';
import { FormikValues, useFormik } from 'formik';
import React, { useState } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { LoginMutation } from '../queries/User';
import routes from '../routes';

const CreateUserScene = () => {
  const [success, setSuccess] = useState<boolean>(false);
  const [loginMutation] = useMutation(LoginMutation);

  const onSubmit = (values: FormikValues) => {
    loginMutation({
      variables: {
        email: values.email,
        password: values.password,
      }
    })
      .then((result) => result.data.authenticateUserWithPassword)
      .then((data) => {
        window.sessionStorage.setItem('token', data.token);
        setSuccess(true);
      })
      .catch((error) => {
        alert(`Error! ${error.message}`);
      });
  };

  const formik = useFormik({
    initialValues: {
      email: undefined,
      password: undefined,
    },
    onSubmit,
  });

  if (success) {
    return <Redirect to={routes.Home} />;
  }

  return (
    <div>
      <p>Log In</p>
      <form onSubmit={formik.handleSubmit}>
        <div>
          <input
            name="email"
            value={formik.values.email || ''}
            required={true}
            placeholder="E-mail"
            onChange={formik.handleChange}
          />
        </div>
        <div>
          <input
            type="password"
            name="password"
            value={formik.values.password || ''}
            required={true}
            placeholder="Password"
            onChange={formik.handleChange}
          />
        </div>
        <div>
          <button type="submit">Submit</button>
          <Link to={routes.Home} className="btn">
            Home
          </Link>
        </div>
      </form>
    </div>
  );
};

export default CreateUserScene;