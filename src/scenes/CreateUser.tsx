import { useMutation } from '@apollo/client';
import { FormikValues, useFormik } from 'formik';
import React, { useState } from 'react';
import { Link, Redirect } from 'react-router-dom';
import { CreateUserMutation } from '../queries/User';
import routes from '../routes';

const CreateUserScene = () => {
  const [success, setSuccess] = useState<boolean>(false);
  const [createUserMutation] = useMutation(CreateUserMutation);

  const onSubmit = (values: FormikValues) => {
    createUserMutation({
      variables: {
        data: values,
      }
    })
      .then((result) => result.data.createUser)
      .then((data) => {
        alert('User was created');
        setSuccess(true);
      })
      .catch((error) => {
        alert(`Error! ${error.message}`);
      });
  };

  const formik = useFormik({
    initialValues: {
      name: undefined,
      email: undefined,
      password: undefined,
    },
    onSubmit,
  });

  if (success) {
    return <Redirect to={routes.Home} />;
  }

  return (
    <div>
      <p>Register</p>
      <form onSubmit={formik.handleSubmit}>
        <div>
          <input
            name="name"
            value={formik.values.name || ''}
            required={true}
            placeholder="Name"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
          />
        </div>
        <div>
          <input
            name="email"
            value={formik.values.email || ''}
            required={true}
            placeholder="E-mail"
            onChange={formik.handleChange}
          />
        </div>
        <div>
          <input
            type="password"
            name="password"
            value={formik.values.password || ''}
            required={true}
            placeholder="Password"
            onChange={formik.handleChange}
          />
        </div>
        <div>
          <button type="submit">Submit</button>
          <Link to={routes.Home} className="btn">
            Home
          </Link>
        </div>
      </form>
    </div>
  );
};

export default CreateUserScene;