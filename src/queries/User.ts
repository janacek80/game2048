import { gql } from '@apollo/client';

export const CreateUserMutation = gql`
  mutation createUser($data: UserCreateInput!) {
    createUser(data: $data) {
      id
      name
    }
  }
`;

export const LoginMutation = gql`
  mutation authenticateUserWithPassword($email: String, $password: String) {
    authenticateUserWithPassword(email: $email, password: $password) {
      token
      item {
        id
        name
      }
    }
  }
`;

export const LoggedUserQuery = gql`
  query authenticatedUser {
    authenticatedUser {
      name
    }
  }
`;