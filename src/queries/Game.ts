import { gql } from '@apollo/client';

export const NewGameQuery = gql`
  query newGame {
    newGame {
      state
      score
      finished
    }
  }
`;

export const ProcessGameMutation = gql`
  mutation processGame($game: GameInput!) {
    processGame(game: $game) {
      state
      score
      finished
    }
  }
`;