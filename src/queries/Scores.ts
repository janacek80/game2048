import { gql } from '@apollo/client';

export const HighScoresQuery = gql`
  query GetHighScores {
    allScores(orderBy: "score_DESC", first: 20) {
      player {
        name
      }
      score
    }
  }
`;

export const CreateScoreMutation = gql`
  mutation createScore($data: ScoreCreateInput) {
    createScore(data: $data) {
      id
      player {
        id
        name
      }
      score
    }
  }
`;